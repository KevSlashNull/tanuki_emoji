# frozen_string_literal: true

module TanukiEmoji
  module Db
    autoload :Gemojione, 'tanuki_emoji/db/gemojione'
    autoload :UnicodeVersion, 'tanuki_emoji/db/unicode_version'
  end
end

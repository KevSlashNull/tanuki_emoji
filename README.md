# Tanuki Emoji

This library helps you implement Emoji support in a ruby application by providing you access to native Emoji character 
information.

We currently provides a pre-indexed set of Emojis, compatible with [Gemojione](https://github.com/bonusly/gemojione)
index from 3.3.0. In the future we expect to cover the most recent version of Unicode.

This gem bundles Emoji assets from [Noto Emoji](https://github.com/googlefonts/noto-emoji), to be used as fallback when
Emoji is not available or not fully supported on target system.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tanuki_emoji'
```

And then execute:

```shell
$ bundle install
```

## Usage

To access the index and list all known Emoji Characters:

```ruby
TanukiEmoji.index.all 
```

To search for an Emoji by it's codepoints (the unicode character):

```ruby
# Find by providing the Emoji Character itself
TanukiEmoji.find_by_codepoints('🐴')
#=> #<TanukiEmoji::Character:horse 🐴(1f434)>

# Find by providing a string representation of the hexadecimal of the codepoint:
TanukiEmoji.find_by_codepoints("\u{1f434}")
#=> #<TanukiEmoji::Character:horse 🐴(1f434)>
```

To search for an Emoji by it's `:alpha_code:`

```ruby
# Find by providing the :alpha_code:
TanukiEmoji.find_by_alpha_code(':horse:')
#=> #<TanukiEmoji::Character:horse 🐴(1f434)>

# It also accepts a `shortcode` (an alpha_code not surrounded by colons)
TanukiEmoji.find_by_alpha_code('horse')
#=> #<TanukiEmoji::Character:horse 🐴(1f434)>
```

To retrieve an alternative image for the character:
```ruby
c = TanukiEmoji.find_by_alpha_code('horse')
#=> #<TanukiEmoji::Character:horse 🐴(1f434)>

c.image_name
#=> "emoji_u1f434.png"

# Use the image_name with bundled assets from Noto Emoji:
File.join(TanukiEmoji.images_path, c.image_name)
#=> "/path/to/tanuki_emoji/app/assets/images/tanuki_emoji/emoji_u1f434.png"
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. 
You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. 
To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, 
which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to 
[rubygems.org](https://rubygems.org).

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

Noto Emoji assets and build tools are [Apache license, version 2.0](https://github.com/googlefonts/noto-emoji/blob/main/LICENSE)
licensed.

Flag images are under
public domain or otherwise excempt from copyright
([more info](https://github.com/googlefonts/noto-emoji/blob/main/third_party/region-flags/LICENSE)).
